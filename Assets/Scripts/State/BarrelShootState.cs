﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class BarrelShootState : State<Barrel, Barrel.BarrelMessage>
{
    private bool canContinue = false;

    public override void Enter( Barrel owner )
    {
        float
            //pressure_factor = GameManager.pressureManager.GetPressureModifier(),
            time_for_shooting = owner.shootingTime /* pressure_factor*/;
        FactionColor
            current_bullet = owner.bulletTable[ owner.selectedChamberIndex ];
        AudioSource
            audio_source = owner.GetComponent<AudioSource>();

        if( current_bullet != FactionColor.EMPTY )
        {
            List<AudioClip>
                shoot_clip_table = GameManager.gameManager.barrelManager.shootTable;

            audio_source.clip = shoot_clip_table[ Random.Range( 0, shoot_clip_table.Count ) ];

            GameManager.gameManager.barrelManager.fireParticleSystem.Play();
        }
        else
        {
            List<AudioClip>
                shoot_empty_clip_table = GameManager.gameManager.barrelManager.shootEmptyTable;

            audio_source.clip = shoot_empty_clip_table[ Random.Range( 0, shoot_empty_clip_table.Count ) ];
        }

        audio_source.Play();

        if( owner.bulletInstanceTable.ContainsKey( owner.selectedChamberIndex ) )
        {
            GameObject.Destroy( owner.bulletInstanceTable[ owner.selectedChamberIndex ].gameObject );
            owner.bulletInstanceTable.Remove( owner.selectedChamberIndex );
        }

        audio_source.Play();

        owner.StartCoroutine( Wait( time_for_shooting ) );
    }

    public override void Execute( Barrel owner, float time_step )
    {
        if( canContinue )
        {
            ++owner.shotBullet;

            owner.bulletTable[ owner.selectedChamberIndex ] = FactionColor.EMPTY;

            owner.ChangeState( typeof( BarrelRevolveState ) );
        }
    }

    public override void Exit( Barrel owner )
    {

    }

    public override bool OnMessage( Barrel owner, Message<Barrel.BarrelMessage> message )
    {
        return false;
    }

    private IEnumerator Wait( float wait_time )
    {
        canContinue = false;

        yield return new WaitForSeconds( wait_time );

        canContinue = true;
    }
}
