﻿using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public List<Ticker> tickerTable;

    void Awake()
    {
        foreach( Ticker ticker in tickerTable )
        {
            ticker.Reset();
        }
    }

    public void ResetTickerTable()
    {
        foreach( Ticker ticker in tickerTable )
        {
            ticker.Reset();
        }
    }

    void Update()
    {
        string info_message = "";

        info_message += GameManager.gameManager.waveInfo;

        float estimated_damage = ( 5 - Mathf.RoundToInt( ( GameManager.gameManager.GetCurrentLife() / GameManager.gameManager.initialLifeCount ) * 5 ) );
        float pressure_level = Mathf.RoundToInt( GameManager.pressureManager.GetCurrentPressure() / 20.0f );
        float canon_dirt_level = Mathf.RoundToInt( GameManager.gameManager.GetDirtyness() / 20.0f );

        if( canon_dirt_level >= 5 )
        {
            info_message += " - CRITICAL - Canon Jammed - Please Clean";
        }

        if( pressure_level >= 5 )
        {
            info_message += " - WARNING - Pressure Level High";
        }
        else if( pressure_level <= 2 )
        {
            info_message += " - WARNING - Pressure Level Low";
        }

        if( estimated_damage >= 4 )
        {
            info_message += " - Critical Damage Level";
        }

        SetTickerTableText( info_message );
    }

    public void SetTickerTableText( string text )
    {
        foreach( Ticker ticker in tickerTable )
        {
            ticker.SetText( text );
        }
    }
}
