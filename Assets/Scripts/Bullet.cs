﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public GameObject
        redColor,
        greenColor,
        blueColor;

    public void SetColor( FactionColor faction_color )
    {
        redColor.SetActive( false );
        greenColor.SetActive( false );
        blueColor.SetActive( false );

        switch( faction_color )
        {
            case FactionColor.RED:
                {
                    redColor.SetActive( true );
                    break;
                }
            case FactionColor.GREEN:
                {
                    greenColor.SetActive( true );
                    break;
                }
            case FactionColor.BLUE:
                {
                    blueColor.SetActive( true );
                    break;
                }
        }
    }
}
