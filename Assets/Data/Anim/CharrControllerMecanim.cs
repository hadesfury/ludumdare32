﻿using System;
using UnityEngine;
using System.Collections;

public class CharrControllerMecanim : MonoBehaviour
{

    private Animator
        anim;
    private Vector2
        velocity = Vector2.zero,
        oldVelocity,
        direction2D,
        characterUp,
        characterRight,
        worldUp,
        worldRight;

    private Vector3
        horiz,
        vert,
        direction;

    private float
        previous_horizontal_axis = 1,
        previous_vertical_axis = 1,
        axis_switch_threshold = 10,
        volume;

    private Camera
        previousCamera;
    private AudioSource
        audio;
	private Rigidbody
		rb;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        volume = audio.volume;
		rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Camera
            current_camera = GameManager.gameManager.GetCamera();
        float
            pad_horiz = Input.GetAxis( "Horizontal" ),
            pad_vert = Input.GetAxis( "Vertical" );
        Vector2
            old_control_vector = new Vector2( previous_horizontal_axis, previous_vertical_axis ),
            new_control_vector = new Vector2( pad_horiz, pad_vert );
        float
            angle_between_old_axis_and_new_axis = Vector2.Angle( new_control_vector, old_control_vector ),
            axis_magnitude_delta = old_control_vector.magnitude - new_control_vector.magnitude;

        if( ( angle_between_old_axis_and_new_axis > axis_switch_threshold )
            || ( Math.Abs( axis_magnitude_delta ) > 0.1f ) )
        {
            previous_horizontal_axis = pad_horiz;
            previous_vertical_axis = pad_vert;
            previousCamera = current_camera;
        }
        else
        {
            pad_horiz = previous_horizontal_axis;
            pad_vert = previous_vertical_axis;
            current_camera = previousCamera;
        }

        //convert pad direction in camera space
        vert = current_camera.transform.forward;
        vert.y = 0;
        Vector3.Normalize( vert );
        horiz = current_camera.transform.right;
        direction = ( horiz * pad_horiz + vert * pad_vert );

        //setting up world axis in local coordinates
        characterUp = new Vector2( transform.forward.x, transform.forward.z );
        characterRight = new Vector2( transform.right.x, transform.right.z );

        worldUp.x = Vector2.Dot( Vector2.up, characterRight );
        worldUp.y = Vector2.Dot( Vector2.up, characterUp );

        worldRight.x = Vector2.Dot( Vector2.right, characterRight );
        worldRight.y = Vector2.Dot( Vector2.right, characterUp );

        //convert world direction vector in character local space
        direction2D = new Vector2( direction.x, direction.z );
        velocity = worldRight * direction2D.x + worldUp * direction2D.y;

		//Deactivated Smoothing, works better on keyboard
//        velocity = ( velocity + oldVelocity ) * 0.5f;
//
//        if( velocity.magnitude < 0.1 )
//        {
//            velocity = oldVelocity * 0.5f;
//        }

        if( velocity.magnitude > 0.1 )
        {
            anim.SetBool( "moving", true );
            audio.volume = volume * rb.velocity.magnitude/5f;
        }
        else
        {
            anim.SetBool( "moving", false );
            audio.volume = 0f;
        }

        anim.SetFloat( "velx", velocity.x );
        anim.SetFloat( "vely", velocity.y );

        oldVelocity = velocity;

        if( Input.GetButtonDown( "Jump" ) /*&& !anim.GetBool("moving")*/ && !anim.GetBool( "jump" ) )
        {
            anim.SetBool( "jump", true );
        }

		RaycastHit hit;
		Debug.DrawRay(transform.position + transform.forward + transform.up, Vector3.down);

		if (Physics.Raycast(transform.position + transform.forward + transform.up, Vector3.down, out hit))
		{
			float distanceToGround = (hit.point - transform.position).y;
//			print(distanceToGround);
			if ( distanceToGround < -0.2f)
			{
				anim.SetBool("falling", true);
				anim.SetBool("climbing", false);
				rb.AddForce(Vector3.up * -10f);
//				Physics.gravity =  new Vector3(0, -20.0F, 0);
			}
			else if ( distanceToGround > 0.15f)
			{
				anim.SetBool("climbing", true);
				if (anim.GetBool("moving")) rb.AddForce(Vector3.up * 10f);
				anim.SetBool("falling", false);
//				Physics.gravity =  new Vector3(0, -9.81F, 0);
			}
			else
			{
				anim.SetBool("climbing", false);
				anim.SetBool("falling", false);
			}
		}

    }
}
