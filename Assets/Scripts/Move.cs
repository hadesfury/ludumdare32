﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

    public float speed;
    public float rotate;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float moveH = Input.GetAxis("Horizontal"),
                moveV = Input.GetAxis("Vertical");

        if (!GameManager.pressureManager.GetEditPressure())
        {
            Vector3 movement = new Vector3(
                                        Mathf.Sin(rb.rotation.eulerAngles.y * Mathf.Deg2Rad),
                                        0.0f,
                                        Mathf.Cos(rb.rotation.eulerAngles.y * Mathf.Deg2Rad)
                                        );
            //if(moveV >=0)
            rb.transform.position += movement * speed * Time.deltaTime * moveV;

            Vector3 eulerAngleVelocity = new Vector3(0.0f, moveH * rotate, 0.0f);

            Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime);

            rb.MoveRotation(rb.rotation * deltaRotation);
        }

    }
}
