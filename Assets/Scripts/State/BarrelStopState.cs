﻿using UnityEngine;
using System.Collections;

public class BarrelStopState : State<Barrel, Barrel.BarrelMessage>
{
    public override void Enter( Barrel owner )
    {

    }

    public override void Execute( Barrel owner, float time_step )
    {

    }

    public override void Exit( Barrel owner )
    {

    }

    public override bool OnMessage( Barrel owner, Message<Barrel.BarrelMessage> message )
    {
        switch( message.mType )
        {
            case Barrel.BarrelMessage.RESET :
            {
                owner.ChangeState( typeof( BarrelWaitState ) );

                return true;
            }
        }
        return false;
    }
}
