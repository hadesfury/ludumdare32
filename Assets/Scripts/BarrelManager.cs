﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using System.Collections;

public class BarrelManager : MonoBehaviour
{
    public float bigBarrelRevolvingTime = 3.0f;

    public GameObject bigBarrelGameObject;
    public Barrel firstGameObjectBarrel;
    public Barrel secondBarrelGameObject;
    public ParticleSystem fireParticleSystem;

    public Bullet bulletPrefab;
    public Transform loadingChamberDock;

    public Animator canonDoor;

    public List<AudioClip> shootTable;
    public List<AudioClip> shootEmptyTable;
    public AudioClip barrelRevolveClip;
    public AudioClip reloadClip;
    public AudioClip bigBarrelRevolveClip;

    private bool isRevolving = false;
    private bool isBroken = false;
    private bool isDoorOpen = false;

    private readonly List<Barrel> barrelTable = new List<Barrel>();

    private const int CHAMBER_COUNT = 6;

    private int currentBarrelShotCount = 4;
    private int loadedBulletCount = 0;

    private Barrel loadingBarrel;
    private Barrel shootingBarrel;


    private void Awake()
    {
        barrelTable.Add( firstGameObjectBarrel );
        barrelTable.Add( secondBarrelGameObject );

        shootingBarrel = barrelTable[ 0 ];
        loadingBarrel = barrelTable[ 1 ];

        //shootingBarrel.currentBarrelState = Barrel.BarrelState.EMPTY;

        foreach( Barrel current_barrel in barrelTable )
        {
            current_barrel.Initialise( bulletPrefab, CHAMBER_COUNT, loadingChamberDock );
        }
    }

    void Update()
    {
        if( GameManager.gameManager.GetDirtyness() >= 100.0f )
        {
            isBroken = true;
        }
        else
        {
            isBroken = false;
        }

        if( !isRevolving && ( currentBarrelShotCount >= CHAMBER_COUNT ) )
        {
            // REVOLVE
            isRevolving = true;
            StartCoroutine( Revolve() );
        }
    }

    public bool CanShoot()
    {
        return !isRevolving && !isBroken && !isDoorOpen && shootingBarrel.CanShoot();
    }

    public bool CanLoadBullet()
    {
        return !isRevolving && ( loadedBulletCount < CHAMBER_COUNT );
    }

    public FactionColor Shoot()
    {
        FactionColor
            shot_bullet = shootingBarrel.Shoot();

        ++currentBarrelShotCount;

        return shot_bullet;
    }

    public IEnumerator Revolve()
    {
        float
            //pressure_factor = GameManager.pressureManager.GetPressureModifier(),
            time_for_revolving = bigBarrelRevolvingTime /* pressure_factor*/;
        AudioSource
            audio_source = GetComponent<AudioSource>();

        // try to stop barrels
        foreach( Barrel barrel in barrelTable )
        {
            while( barrel.GetCurrentState() != typeof( BarrelStopState ) )
            {
                barrel.HandleMessage( new Message<Barrel.BarrelMessage>( Barrel.BarrelMessage.INTERRUPT ) );
                yield return null;
            }
        }

        audio_source.clip = bigBarrelRevolveClip;
        audio_source.pitch = 1 / time_for_revolving;
        audio_source.Play();

        loadingBarrel.Stop();

        isRevolving = true;

        GetComponent<Animator>().SetTrigger( "switch" );

        yield return new WaitForSeconds( time_for_revolving );

        Barrel tmp_barrel = loadingBarrel;

        loadingBarrel = shootingBarrel;
        shootingBarrel = tmp_barrel;
        //shootingBarrel.currentBarrelState = Barrel.BarrelState.WAITING;
        //loadingBarrel.currentBarrelState = Barrel.BarrelState.WAITING;
        loadedBulletCount = 0;
        currentBarrelShotCount = 0;

        isRevolving = false;

        // release barrels
        foreach( Barrel barrel in barrelTable )
        {
            barrel.Reset();
        }
    }

    public bool LoadBullet( FactionColor bullet_color )
    {
        bool can_load_bullet = CanLoadBullet();

        if( can_load_bullet )
        {
            loadingBarrel.LoadBullet( bullet_color );
            ++loadedBulletCount;
        }

        return can_load_bullet;
    }

    public string GetDebugMessage()
    {
        string debug_message = "Big Barrel State\n";

        debug_message += "isRevolving = " + isRevolving + "\n";
        debug_message += "isBroken = " + isBroken + "\n";
        debug_message += "isDoorOpen = " + isDoorOpen + "\n";
        debug_message += "Shooting Barrel : " + shootingBarrel.GetDebugMessage() + "\n";
        debug_message += "Loadable Barrel : " + loadingBarrel.GetDebugMessage() + "\n";

        return debug_message;
    }

    public void OpenCanonDoor()
    {
        if( !isDoorOpen )
        {
            isDoorOpen = true;
            canonDoor.SetTrigger( "switch" );
        }
        else
        {
            isDoorOpen = false;
            canonDoor.SetTrigger( "switch" );
        }
    }
}
