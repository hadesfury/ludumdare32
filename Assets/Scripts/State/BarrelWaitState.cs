﻿using UnityEngine;
using System.Collections;

public class BarrelWaitState : State<Barrel, Barrel.BarrelMessage>
{
    public override void Enter( Barrel owner )
    {

    }

    public override void Execute( Barrel owner, float time_step )
    {

    }

    public override void Exit( Barrel owner )
    {

    }

    public override bool OnMessage( Barrel owner, Message<Barrel.BarrelMessage> message )
    {
        switch( message.mType )
        {
            case Barrel.BarrelMessage.SHOOT:
                {
                    owner.ChangeState( typeof( BarrelShootState ) );

                    return true;
                }
            case Barrel.BarrelMessage.LOAD:
                {
                    owner.ChangeState( typeof( BarrelLoadState ) );

                    return true;
                }
            case Barrel.BarrelMessage.INTERRUPT:
                {
                    owner.ChangeState( typeof( BarrelStopState ) );

                    return true;
                }
        }

        return false;
    }
}
