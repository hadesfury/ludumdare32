﻿using UnityEngine;
using System.Collections;

public class SyncWithPlayer : MonoBehaviour {

    Camera camToFace;

    //*/
    void Awake()
    {
        camToFace = Camera.allCameras[0];
    }

    //*/
    // Update is called once per frame
    void FixedUpdate()
    {

        UpdateCam();
        Quaternion camAngle = AngleCam();
        this.transform.rotation = camAngle;
    }

    //*/
    void UpdateCam()
    {
        camToFace = Camera.allCameras[0];
    }

    Quaternion AngleCam()
    {
        return camToFace.transform.rotation;
    }
    //*/
}
