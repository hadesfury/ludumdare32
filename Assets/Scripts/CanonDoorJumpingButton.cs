﻿using DG.Tweening;
using UnityEngine;
using System.Collections;

public class CanonDoorJumpingButton : BulletJumpingButton
{
    protected override IEnumerator PressButton()
    {
        currentState = ButtonState.USED;

        //if( GameManager.gameManager.barrelManager.CanOpenCanonDoor() )
        {
            yield return new WaitForSeconds( pressDelayTime );
            gameObject.transform.DOLocalMoveZ( -pressDepth, pressDownTime ).SetEase( Ease.OutCubic );
            yield return new WaitForSeconds( pressDownTime );
            GameManager.gameManager.barrelManager.OpenCanonDoor(  );
            gameObject.transform.DOLocalMoveZ( 0, pressUpTime );
            yield return new WaitForSeconds( pressUpTime );
        }
        currentState = ButtonState.ACTIVE;
    }

//    public void Enter()
//    {
//        currentState = ButtonState.ACTIVE;
//        GameManager.gameManager.player.DisplayBubbleText( GameManager.gameManager.jumpText );
//        gameObject.transform.DOLocalMoveY( -activeDepth, 0.1f ).SetEase( Ease.OutCubic );
//    }

//    public void Exit()
//    {
//        currentState = ButtonState.INACTIVE;
//        GameManager.gameManager.player.HideBubble();
//        gameObject.transform.DOLocalMoveY( activeDepth, 0.1f ).SetEase( Ease.OutCubic );
//    }
}
