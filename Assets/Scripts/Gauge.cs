﻿using UnityEngine;
using System.Collections;

public class Gauge : MonoBehaviour
{
    void Update()
    {
        float current_pressure = GameManager.pressureManager.GetCurrentPressure();
        Vector3 current_scale = gameObject.transform.localScale;

        current_scale.z = current_pressure / 100.0f;

        gameObject.transform.localScale = current_scale;
    }
}
