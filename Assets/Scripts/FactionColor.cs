﻿using UnityEngine;
using System.Collections;

public enum FactionColor 
{
    EMPTY,
    RED,
    GREEN,
    BLUE,
}
