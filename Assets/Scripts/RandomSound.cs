﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class RandomSound : MonoBehaviour
{
    public List<AudioClip> soundTable;

    void Update()
    {
        if( !GetComponent<AudioSource>().isPlaying )
        {
            PlayRandowSound();
        }
    }

    public void PlayRandowSound()
    {
        int sound_index = Random.Range( 0, soundTable.Count );

        GetComponent<AudioSource>().clip = soundTable[ sound_index ];
        GetComponent<AudioSource>().Play();
    }
}
