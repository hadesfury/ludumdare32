﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ticker : MonoBehaviour
{
    public float initialTickerSize;
    public Text tickerText;
    public int tickerSpeed;
    public int tickerLedSize = 1000;

    private float tickerMovement = 0;

    void Awake()
    {
        Reset();
    }

    public void Reset()
    {
        ( ( RectTransform ) tickerText.transform ).anchoredPosition = new Vector3( initialTickerSize, 0, 0 );
    }

    public void SetText(string text)
    {
        tickerText.text = text;
    }

    void Update()
    {
        //tickerMovement += tickerSpeed * Time.deltaTime;
        //if( tickerMovement < -1 )
        //{
        //    float movemement = Mathf.Floor( tickerMovement * tickerLedSize ) / tickerLedSize;
        //    tickerText.transform.Translate( movemement, 0, 0 );

        //    tickerMovement -= movemement;
        //}
        tickerText.transform.Translate( tickerSpeed * Time.deltaTime, 0, 0 );

        if( ( ( RectTransform ) tickerText.transform ).anchoredPosition.x < -( ( RectTransform ) tickerText.transform ).sizeDelta.x )
        {
            Reset();
        }
    }
}
