﻿using DG.Tweening;
using UnityEngine;
using System.Collections;

public class BarrelRevolveState : State<Barrel, Barrel.BarrelMessage>
{
    private bool canContinue = false;

    public override void Enter( Barrel owner )
    {
        float
            pressure_factor = GameManager.pressureManager.GetPressureModifier(),
            revolution_time = owner.barrelRevolvingTime * pressure_factor;
        Vector3
            next_rotation = Vector3.zero;
        AudioSource
            audio_source = owner.GetComponent<AudioSource>();

        audio_source.clip = GameManager.gameManager.barrelManager.barrelRevolveClip;
        audio_source.Play();

        next_rotation.x += 360.0f / ( float ) owner.chamberCount;

        owner.gameObject.transform.DOLocalRotate( next_rotation, revolution_time, RotateMode.LocalAxisAdd );

        owner.StartCoroutine( Wait( revolution_time ) );
    }

    public override void Execute( Barrel owner, float time_step )
    {

        if( canContinue )
        {
            owner.selectedChamberIndex = ( owner.selectedChamberIndex + 1 ) % owner.chamberCount;
            
            owner.ChangeState( typeof( BarrelWaitState ) );
        }
    }

    public override void Exit( Barrel owner )
    {

    }

    public override bool OnMessage( Barrel owner, Message<Barrel.BarrelMessage> message )
    {
        return false;
    }

    private IEnumerator Wait( float wait_time )
    {
        canContinue = false;

        yield return new WaitForSeconds( wait_time );

        canContinue = true;
    }
}
