﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class Barrel : MonoBehaviour
{
    public float shootingTime = 1.0f;
    public float barrelRevolvingTime = 1.0f;
    public float bulletLoadingTime = 2.0f;

    internal Bullet bulletPrefab;
    private Transform loadingChamber;

    internal int selectedChamberIndex = 0;
    internal int chamberCount = 6;
    internal int shotBullet = 0;
    internal Bullet bulletToLoad;
    internal FactionColor bulletColorToLoad = FactionColor.EMPTY;

    internal readonly List<FactionColor> bulletTable = new List<FactionColor>();
    internal readonly Dictionary<int, Bullet> bulletInstanceTable = new Dictionary<int, Bullet>();
    internal readonly Queue<FactionColor> bulletToLoadTable = new Queue<FactionColor>();

    private StateMachine<Barrel, BarrelMessage> barrelStateMachine;

    public enum BarrelMessage
    {
        SHOOT,
        LOAD,
        INTERRUPT,
        RESET
    }

    //public bool canShoot { get; private set; }

    void Awake()
    {
        barrelStateMachine = new StateMachine<Barrel, BarrelMessage>( this );
        barrelStateMachine.RegisterState( new BarrelLoadState() );
        barrelStateMachine.RegisterState( new BarrelRevolveState() );
        barrelStateMachine.RegisterState( new BarrelStopState() );
        barrelStateMachine.RegisterState( new BarrelShootState() );
        barrelStateMachine.RegisterState( new BarrelWaitState() );

        barrelStateMachine.SetCurrentState( typeof( BarrelWaitState ) );
    }

    public void ChangeState( Type new_state )
    {
        barrelStateMachine.ChangeState( new_state );
    }

    public bool HandleMessage( Message<BarrelMessage> message )
    {
        return barrelStateMachine.HandleMessage( message );
    }

    public Type GetCurrentState()
    {
        return barrelStateMachine.GetCurrentState();
    }

    public void Initialise( Bullet bullet_prefab, int chamber_count, Transform loading_chamber )
    {
        bulletPrefab = bullet_prefab;
        chamberCount = chamber_count;
        loadingChamber = loading_chamber;

        for( int bullet_index = 0;bullet_index < chamberCount;bullet_index++ )
        {
            bulletTable.Add( FactionColor.EMPTY );
        }
    }

    public void LoadBullet( FactionColor bullet_color )
    {
        bulletToLoadTable.Enqueue( bullet_color );
    }

    public void Update()
    {
        barrelStateMachine.Update( Time.deltaTime );

        if( GetCurrentState() == typeof( BarrelWaitState ) )
        {
            if( bulletToLoadTable.Count > 0 )
            {
                HandleMessage( new Message<BarrelMessage>( BarrelMessage.LOAD ) );
            }
        }
    }

    public bool CanShoot()
    {
        return GetCurrentState() == typeof( BarrelWaitState );
    }

    public FactionColor Shoot()
    {
        FactionColor
            current_bullet = bulletTable[ selectedChamberIndex ];

        HandleMessage( new Message<BarrelMessage>( BarrelMessage.SHOOT ) );

        if( GetCurrentState() != typeof( BarrelShootState ) )
        {
            current_bullet = FactionColor.EMPTY;
        }

        return current_bullet;
    }

    public string GetDebugMessage()
    {
        string debug_message = "State : " + GetCurrentState() + "\n";
        debug_message += "Selected chamber : " + selectedChamberIndex + "\n";

        foreach( FactionColor current_bullet_color in bulletTable )
        {
            debug_message += current_bullet_color + ", ";
        }

        return debug_message;
    }

    public void Stop()
    {
        bulletToLoadTable.Clear();
    }

    public void Reset()
    {
        HandleMessage( new Message<BarrelMessage>( BarrelMessage.RESET ) );
    }
}
