﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Wave
{

    public int enemyCount;
    public static int size, min, max, wave_nb;
    private static bool level = false;
    public int[] enemyCountArray;
    public static List<Enemy> enemyTable = new List<Enemy>();

    private int waveIndex;

    public Wave( int wave_index )
    {
        waveIndex = wave_index;

        if( wave_index % 3 == 0 )
        {
            level = true;
            if( ( wave_index - 3 ) % 6 == 0 && wave_index >= 3 ) // Starting level 2, for each 6 wave nbColor++
                Enemy.GetNbColor++;
            min = Enemy.GetNbColor;
            max += Enemy.GetNbColor;
            if( max > 12 )
                max = 12;
        }
        if( wave_index < 3 )
        {
            Enemy.GetNbColor = 1;
            min = wave_index + 1;
            max = wave_index + 1;
        }
        enemyCount = UnityEngine.Random.Range( min, max );
        size = Enum.GetNames( typeof( FactionColor ) ).Length - 1; //To Remove FactionColor.EMPTY
        Generate( wave_index );
    }

    public int GetEnemyCount()
    {
        return enemyTable.Count;
    }

    public static bool Level
    {
        get { return level; }
        set { level = value; }
    }

    public int Shoot( FactionColor shot_bullet_color )
    {
        Enemy
            enemy_shot = enemyTable[ 0 ];
        int
            score = 0;

        foreach( Enemy enemy in enemyTable )
        {
            if( enemy.GetColor() == shot_bullet_color )
            {
                enemy_shot = enemy;
                break;
            }
        }


        enemy_shot.ApplyDamage( shot_bullet_color );

        if( enemy_shot.IsKilled() )
        {
            score += enemy_shot.GetScore();
            enemyTable.Remove( enemy_shot );
        }

        return score;
    }

    private void Generate( int wave_index )
    {
        wave_nb = wave_index + 1;
        int nbcol = wave_index % size;
        Enemy.GetNbColor = nbcol;
        //        level = wave_index % Enemy.GetNbColor() + 1;
        enemyCountArray = new int[ size ];
        for( int j = 0;j < enemyCountArray.Length;j++ )
        {
            enemyCountArray[ j ] = 0;
        }

        //add enemy to enemyCountArray
        int i = 0;
        while( i < enemyCount )
        {
            int index = UnityEngine.Random.Range( 0, Enemy.GetNbColor );
            enemyCountArray[ index ] = enemyCountArray[ index ] + 1;
            i++;
        }

        for( int j = 0;j < size;j++ )
        {
            i = 0;
            while( i < enemyCountArray[ j ] )
            {
                enemyTable.Add( new Enemy( ( FactionColor ) ( j + 1 ) ) );
                i++;
            }
        }

        enemyCount++;
    }

    public string GetWaveMessage()
    {
        string
            wave_message = "Wave - " + waveIndex + " - ";
        int[]
            enemy_count_array = new int[ size ];

        for( int i = 0;i < size;i++ )
        {
            enemy_count_array[ i ] = enemyTable.FindAll( x => x.GetColor() == ( ( FactionColor ) ( i + 1 ) ) ).Count;
        }

        for( int i = 0;i < enemy_count_array.Length;i++ )
        {
            if( i > 0 )
            {
                wave_message += " - ";
            }
            if( enemy_count_array[ i ] != 0 )
            {
                string 
                    str = "" + ( FactionColor ) ( i + 1 );

                wave_message += "" + str + " x " + enemy_count_array[ i ];
            }
        }


        return wave_message;
    }
}

