﻿using DG.Tweening;
using UnityEngine;
using System.Collections;

public class BulletJumpingButton : MonoBehaviour
{
    public FactionColor bulletColor;
    public float activeDepth = 0.5f;
    public float pressDepth = 3.0f;
    public float pressDelayTime = 1.0f;
    public float pressDownTime = 1.0f;
    public float pressUpTime = 1.0f;

    protected ButtonState currentState = ButtonState.INACTIVE;

    protected enum ButtonState
    {
        INACTIVE,
        ACTIVE,
        USED
    }

    public void Enter()
    {
        currentState = ButtonState.ACTIVE;
        GameManager.gameManager.player.DisplayBubbleText( GameManager.gameManager.jumpText );
        gameObject.transform.DOLocalMoveZ( -activeDepth, 0.1f ).SetEase( Ease.OutCubic );
    }

    public void Exit()
    {
        currentState = ButtonState.INACTIVE;
        GameManager.gameManager.player.HideBubble();
        gameObject.transform.DOLocalMoveZ( activeDepth, 0.1f ).SetEase( Ease.OutCubic );
    }

    private void Update()
    {
        if( Input.GetButtonDown( "Jump" ) )
        {
            //print( "isOnButton : " + isOnButton );
            if( currentState == ButtonState.ACTIVE )
            {
                StartCoroutine( PressButton() );
            }
        }
    }

    protected virtual IEnumerator PressButton()
    {
        currentState = ButtonState.USED;

        if( GameManager.gameManager.barrelManager.CanLoadBullet() )
        {
            yield return new WaitForSeconds( pressDelayTime );
            gameObject.transform.DOLocalMoveZ( -pressDepth, pressDownTime ).SetEase( Ease.OutCubic );
            yield return new WaitForSeconds( pressDownTime );
            GameManager.gameManager.barrelManager.LoadBullet( bulletColor );
            gameObject.transform.DOLocalMoveZ( 0, pressUpTime );
            yield return new WaitForSeconds( pressUpTime );
        }
        currentState = ButtonState.ACTIVE;
    }
}
