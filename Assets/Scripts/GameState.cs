﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    START_MENU,
    WAITING_NEXT_WAVE,
    WAVE_IN_PROGRESS,
    GAME_OVER
}
