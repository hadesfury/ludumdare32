﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;

public class GameManager : MonoBehaviour
{
    public string activableText = "Press Ctrl";
    public string jumpText = "Press Space to jump here";
    public bool displayDebugPanel = true;
    public int initialLifeCount = 100;
    public int addedDirt = 5;
    public float cleaningSpeed = 10.0f;

    public float timeBetweenWave = 60.0f;
    public float timeBetweenEnemyShot = 5.0f;
    public float deltaDirt;
    public float initialDirt;
    public float waveDuration;

    public string waveInfo;

    public Player player;

    public BarrelManager barrelManager;
    public Animator loadingMechanism;
    public Transform loadingMechanismBulletDock;
    public Animator redBulletRack;
    public Animator greenBulletRack;
    public Animator blueBulletRack;

    public List< GameObject > explosionTable; 

    public static GameManager gameManager;
    public static GuiManager guiManager;
    public static PressureManager pressureManager;
    public static LevelManager levelManager;

    private int currentWaveIndex = 0;

    private bool attend = true;

    private int enemyCount = 0;
    private int currentLifeCount;
    private int currentScore = 0;
    private Stat stat = new Stat();

    private Camera currentCamera;

    private float timeSinceLastWave = 0;
    private float dirtyness = 0;

    private GameState currentGameState = GameState.START_MENU;

    private float shootingTime;
    private float timeBeforeShoot;
    private float timeBtwWave;

    private FactionColor nextBullet;

    private float cleaningStartTime;
    private string playerName;

    void Awake()
    {
        //guiManager.gameStartPanel.gameObject.SetActive( true );
        gameManager = this;
        guiManager = GetComponent<GuiManager>();
        //barrelManager = GetComponent<BarrelManager>();
        pressureManager = GetComponent<PressureManager>();
        levelManager = GetComponent<LevelManager>();
        currentLifeCount = initialLifeCount;
        dirtyness = initialDirt;
        ClampDirt();
        guiManager.gameStartPanel.gameObject.SetActive( true );
        guiManager.gameOverPanel.gameObject.SetActive( false );
        SetCurrentCamera( Camera.main );

        guiManager.debugPanel.gameObject.SetActive( false );
        levelManager.tickerTable[ 0 ].gameObject.SetActive( false );

#if UNITY_EDITOR
        guiManager.debugPanel.gameObject.SetActive( true );
        levelManager.tickerTable[ 0 ].gameObject.SetActive( true );
#endif

        guiManager.gameStartScreenInputField.text = PlayerPrefs.GetString( "playerName", "" );

        Time.timeScale = 0;
    }

    void Update()
    {
        timeBeforeShoot += Time.deltaTime;
        timeBtwWave += Time.deltaTime;
        shootingTime = F( pressureManager.GetCurrentPressure() );
        if( shootingTime - timeBeforeShoot < 0 )
            timeBeforeShoot = 0;
        if( currentGameState == GameState.WAITING_NEXT_WAVE )
        {
            timeSinceLastWave += Time.deltaTime;
            //shootingTime = shootingTime - timeBeforeShoot;

            if( timeSinceLastWave >= timeBetweenWave )
            {
                StartCoroutine( StartNextWave() );
            }
            else
            {
                waveInfo = String.Format( "Next Wave in {0:F1} seconds", ( timeBetweenWave - timeSinceLastWave ) );
                //levelManager.SetTickerTableText( waveInfo );
            }
        }

        if( displayDebugPanel )
        {
            guiManager.debugPanel.gameObject.SetActive( true );
            guiManager.debugText.text = "Debug\n";
            guiManager.debugText.text += "Enemy : " + enemyCount + "\n";
            guiManager.debugText.text += "Life : " + currentLifeCount + "\n";
            guiManager.debugText.text += "Level : " + Wave.Level + "\n";
            //guiManager.debugText.text += String.Format("Next Wave in {0:F2} seconds ",( waveDuration - timeBtwWave)) + "\n";
            guiManager.debugText.text += "Score : " + currentScore + "\n";
            //guiManager.debugText.text += String.Format("End of Wave in : {0:F1} seconds", (waveDuration -(Time.time - waveStart))) + "\n";
            //Time.time - waveStart == waveDuration
            guiManager.debugText.text += pressureManager.GetDebugMessage() + "\n";
            guiManager.debugText.text += "dirt level : " + dirtyness + "\n";//"«"//pressureManager.GetDebugMessage() + "\n";
            guiManager.debugText.text += "Killed enemies : " + stat.killedEnemyCount + "\n";
            //guiManager.debugText.text += "Propreté : " + ( initialDirt - dirtyness ) + "\n";
            //guiManager.debugText.text += "F(Pression) : " + F(pressureManager.GetCurrentPressure()) + "\n";
            guiManager.debugText.text += "GameState : " + currentGameState + "\n";
            //guiManager.debugText.text += String.Format("Next Shoot in : {0:F1} seconds", (shootingTime - timeBeforeShoot)) + "\n";
            guiManager.debugText.text += "Next Bullet : " + nextBullet + "\n";


            guiManager.debugText.text += barrelManager.GetDebugMessage() + "\n";
        }
        else
        {
            guiManager.debugPanel.gameObject.SetActive( false );
        }

        //if( barrelManager.CanShoot() )
        //{
        //    barrelManager.ForceShoot();
        //}
        if( ( Input.GetKey( KeyCode.LeftControl ) || Input.GetKey( KeyCode.RightControl ) )
            && Input.GetKeyUp( KeyCode.Backspace ) )
        {
            displayDebugPanel = !displayDebugPanel;
        }

    }

    void OnGUI()
    {
    }

    public void SetNextBullet( FactionColor other )
    {
        nextBullet = other;
    }

    public Camera GetCamera()
    {
        return currentCamera;
    }

    public void SetCurrentCamera( Camera current_camera )
    {
        currentCamera = current_camera;
        Camera.allCameras[ 0 ].gameObject.SetActive( false );
        current_camera.gameObject.SetActive( true );
        currentCamera = current_camera;

    }

    float F( float x )
    {
        return ( float ) ( -1 * Math.Log( 0.0031 * x ) );
    }

    IEnumerator EndWave()
    {
        timeBtwWave = 0;
        yield return new WaitForSeconds( waveDuration );
        Wave.Level = true;
    }

    IEnumerator StartNextWave()
    {
        Wave
            wave = new Wave( currentWaveIndex );

        currentGameState = GameState.WAVE_IN_PROGRESS;

        StartCoroutine( EnemyDamageCoroutine( wave ) );

        while( wave.GetEnemyCount() > 0 && (currentGameState == GameState.WAVE_IN_PROGRESS) )
        {
            waveInfo = wave.GetWaveMessage();
            while( !barrelManager.CanShoot() )
            {
                yield return null;
            }

            FactionColor
                shot_bullet_color = barrelManager.Shoot();

            pressureManager.ReducePressure( pressureManager.pressureShotDecreaseSpeed );

            if( shot_bullet_color != FactionColor.EMPTY )
            {
                currentScore += wave.Shoot( shot_bullet_color );
                dirtyness += addedDirt;
            }
        }

        ++currentWaveIndex;
        levelManager.ResetTickerTable();
        currentGameState = GameState.WAITING_NEXT_WAVE;
        timeSinceLastWave = 0;
    }

    private IEnumerator EnemyDamageCoroutine(Wave wave)
    {
        yield return new WaitForSeconds( timeBetweenEnemyShot );
        while( wave.GetEnemyCount() > 0 && (currentGameState == GameState.WAVE_IN_PROGRESS) )
        {
            currentLifeCount -= wave.GetEnemyCount();

            yield return StartCoroutine( PlayExplosions( wave.GetEnemyCount() ) );

            if( currentLifeCount <= 0 )
            {
                GameOver();
            }

            yield return new WaitForSeconds( timeBetweenEnemyShot );
        }
    }

    private IEnumerator PlayExplosions( int explosion_count )
    {
        int explosion_index = 0;
        foreach( GameObject explosion in explosionTable )
        {
            currentCamera.DOShakePosition( 0.1f, 1.0f, 10, 90.0f );
            explosion.GetComponent< AudioSource >().Play();

            foreach( ParticleSystem particle_system in explosion.GetComponentsInChildren<ParticleSystem>() )
            {
                particle_system.Play();
            }
            yield return new WaitForSeconds( 0.1f );

            ++explosion_index;

            if( explosion_index >= explosion_count )
            {
                break;
            }
        }

    }

    IEnumerator SendScore()
    {
        string
            json = "{\"playerName\": \"" + playerName + "(POST COMPO)" + "\",\"score\": \"" + currentScore + "\"}";
        Dictionary<string, string>
            header_table = new Dictionary<string, string>();

        header_table.Add( "Content-Type", "application/json" );

        byte[] 
            json_data = Encoding.ASCII.GetBytes( json.ToCharArray() );
        WWW 
            www = new WWW( @"http://dev-ludumdare32.rhcloud.com/score", json_data, header_table );

        yield return www;
        string 
            www_result = "" + www + "\n";

        print( www_result );
    }

    private void GameOver()
    {
        currentGameState = GameState.GAME_OVER;
        Time.timeScale = 0;
        guiManager.gameOverPanel.gameObject.SetActive( true );
        guiManager.scoreText.text = String.Format( "Score : {0}", currentScore );
        StartCoroutine( SendScore() );
    }

    public void Restart()
    {
        Application.LoadLevel( 0 );
    }

    public void StartGame()
    {
        string name_input_field = guiManager.gameStartScreenInputField.text;

        guiManager.gameStartPanel.gameObject.SetActive( false );

        if( name_input_field == "" )
        {
            name_input_field = SystemInfo.deviceName;//"Guest";
        }

        playerName = name_input_field;

        PlayerPrefs.SetString( "playerName", playerName );

        currentGameState = GameState.WAITING_NEXT_WAVE;
        Time.timeScale = 1;
    }

    #region Propreté
    public void InteractDirt()
    {
        string str = "Press Space to Clean";
        ClampDirt();
        if( Input.GetButton( "Jump" ) && attend )
        {
            dirtyness -= deltaDirt;
            attend = false;
        }
        if( Input.GetButtonUp( "Jump" ) )
            attend = true;

        player.DisplayBubbleText( str );
    }

    void ClampDirt()
    {
        float coeff = 0.1f;
        deltaDirt = dirtyness * coeff;
        deltaDirt = ( float ) Math.Round( deltaDirt, 2 );
        if( deltaDirt < 1 )
            deltaDirt = 1;
        if( deltaDirt > initialDirt * coeff )
            deltaDirt = initialDirt * coeff;
        if( dirtyness > initialDirt )
            dirtyness = initialDirt;
        if( dirtyness < 0 )
            dirtyness = 0;
    }

    #endregion

    public void StartCanonClean()
    {
        cleaningStartTime = Time.time;
    }

    public void StopCanonClean()
    {
        float cleaning_time = Time.time - cleaningStartTime;

        dirtyness = Mathf.Clamp( dirtyness - cleaning_time * cleaningSpeed, 0, 100 );
    }

    public float GetDirtyness()
    {
        return dirtyness;
    }

    public float GetCurrentLife()
    {
        return currentLifeCount;
    }
}
