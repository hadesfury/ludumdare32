﻿using UnityEngine;
using System.Collections;
using System;


public class Enemy
{
    private FactionColor 
        color;
    private int 
        shotCount = 0,
        lifePoint = Enum.GetNames(typeof(FactionColor)).Length -1;
    private bool 
        isKilled = false;

    static int nbColor = 1;
 
    public Enemy()
    {
        color = (FactionColor) UnityEngine.Random.Range(1, sizeof(FactionColor));
        isKilled = false;
        shotCount = 0;
    }

    public Enemy(FactionColor col)
    {
        color = col;
        isKilled = false;
        shotCount = 0;
    }

    public int GetScore()
    {
        int 
            score = 500 - shotCount * 100;

        return score;
    }

    public void ApplyDamage( FactionColor shot_bullet_color )
    {
        int
            damage = Enemy.Damage( ( int ) GetColor(), ( int ) ( shot_bullet_color ) );

        lifePointCount -= damage;
        ++shotCount;

        if( lifePointCount <= 0 )
        {
            isKilled = true;
        }
    }

    public bool IsKilled()
    {
        return isKilled;
    }


    public static int Damage(int i,int j)
    {
        int size = Enum.GetNames(typeof(FactionColor)).Length -1;
        if (i == j)
            return size;
        else
        {
            return size - Math.Abs(i - j);
        }
    }

    public static int GetNbColor
    {
        get { return nbColor; }
        set { 
            nbColor = value;
            if(nbColor > Enum.GetNames(typeof(FactionColor)).Length)
                nbColor = Enum.GetNames(typeof(FactionColor)).Length;
            if (nbColor < 1)
                nbColor = 1;
        }
    }

    public int lifePointCount
    {
        get { return lifePoint; }
        set { lifePoint = value; }
    }
    public void SetColor(FactionColor col)
    {
        color = col;
    }

    public FactionColor GetColor()
    {
        return color;
    }

    public override string ToString() 
    {
        return "" + color + " - " + lifePoint;
    }
}