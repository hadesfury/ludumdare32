﻿using UnityEngine;
using System.Collections;

public class CleaningTrigger : MonoBehaviour
{
    public ParticleSystem smokeParticleSystem;

    private bool doesClean = false;

    void Awake()
    {
        smokeParticleSystem.Stop();
    }

    void OnTriggerEnter( Collider other )
    {
        doesClean = true;
        smokeParticleSystem.Play();
        GameManager.gameManager.StartCanonClean();
    }

    void OnTriggerExit( Collider other )
    {
        doesClean = false;
        smokeParticleSystem.Stop();
        GameManager.gameManager.StopCanonClean();
    }
}
