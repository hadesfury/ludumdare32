﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour 
{
    public Canvas infoCanvas;
    public Text infoText;

    // Use this for initialization
    void Start()
    {
        HideBubble();
    }

    public void DisplayBubbleText( string bubble_text )
    {
        infoCanvas.gameObject.SetActive( true );
        infoText.text = bubble_text;
    }

    public void HideBubble()
    {
        infoCanvas.gameObject.SetActive( false );
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Activable>() != null)
        {
            //GetComponent<InfoBulle>().RewriteInfoBulle("" + Camera.allCameras[0].name);
            other.GetComponent<Activable>().uEvent.Invoke();
        }
    }
    void OnTriggerExit(Collider other)
    {
        HideBubble();
    }
}
