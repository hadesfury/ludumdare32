﻿using UnityEngine;
using System.Collections;

public class BarrelLoadState : State<Barrel, Barrel.BarrelMessage>
{
    private bool
        canContinue = false;
    private Bullet
        bulletInstance;

    public override void Enter( Barrel owner )
    {
        //LOAD
        {
            AudioSource
                audio_source = owner.GetComponent<AudioSource>();
            FactionColor
                bullet_color = owner.bulletToLoadTable.Dequeue();

            bulletInstance = GameObject.Instantiate( owner.bulletPrefab );

            GameManager.gameManager.loadingMechanism.SetTrigger( ( "load" + bullet_color ).ToLower() );

            owner.bulletTable[ owner.selectedChamberIndex ] = bullet_color;


            bulletInstance.SetColor( bullet_color );

            owner.bulletInstanceTable.Add( owner.selectedChamberIndex, bulletInstance );
            //bullet_instance.transform.position = GameManager.gameManager.loadingMechanismBulletDock.position;
            //bullet_instance.transform.rotation = GameManager.gameManager.loadingMechanismBulletDock.rotation;
            bulletInstance.transform.SetParent( GameManager.gameManager.loadingMechanismBulletDock.transform, false );


            audio_source.clip = GameManager.gameManager.barrelManager.reloadClip;
            audio_source.Play();

            switch( bullet_color )
            {
                case FactionColor.RED:
                    {
                        GameManager.gameManager.redBulletRack.SetTrigger( "load" );
                        break;
                    }
                case FactionColor.GREEN:
                    {
                        GameManager.gameManager.greenBulletRack.SetTrigger( "load" );
                        break;
                    }
                case FactionColor.BLUE:
                    {
                        GameManager.gameManager.blueBulletRack.SetTrigger( "load" );
                        break;
                    }
            }

            owner.StartCoroutine( Wait( owner.bulletLoadingTime ) );
        }
    }

    public override void Execute( Barrel owner, float time_step )
    {
        if( canContinue )
        {
            bulletInstance.transform.SetParent( owner.gameObject.transform, true );

            owner.ChangeState( typeof( BarrelRevolveState ) );
        }
    }

    public override void Exit( Barrel owner )
    {

    }

    public override bool OnMessage( Barrel owner, Message<Barrel.BarrelMessage> message )
    {
        return false;
    }

    private IEnumerator Wait( float wait_time )
    {
        canContinue = false;

        yield return new WaitForSeconds( wait_time );

        canContinue = true;
    }
}
