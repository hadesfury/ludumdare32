
using System.Collections.Generic;

public class Message<_message_type_>
{
	
	public Message()
    {

    }

    public Message( _message_type_ message_type )
    {
		mType = message_type;
    }

    public Message( _message_type_ message_type, List<object> extra_info )
    {
        mType = message_type;
        mExtraInfo = extra_info;
    }

    // ~~

    Message( Message<_message_type_> other )
    {
        mDelay = other.mDelay;
        mDispachTime = other.mDispachTime;
        mSenderId = other.mSenderId;
        mReceiverId = other.mReceiverId;
        mType = other.mType;
        mExtraInfo = other.mExtraInfo;
    }

    // .. ATTRIBUTES

    public double
        mDelay = 0.0,
        mDispachTime = 0.0;
    public int
        mSenderId = -1,
        mReceiverId = -1;
    public _message_type_
        mType;
    public List<object>
        mExtraInfo;	
	
};
