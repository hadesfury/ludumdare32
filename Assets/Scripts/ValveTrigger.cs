﻿using UnityEngine;
using System.Collections;

public class ValveTrigger : MonoBehaviour
{
    private bool canUseValve = false;

    void OnTriggerEnter( Collider other )
    {
        canUseValve = true;
        GameManager.gameManager.player.DisplayBubbleText( GameManager.gameManager.activableText );
    }

    void OnTriggerExit( Collider other )
    {
        canUseValve = false;
        GameManager.gameManager.player.HideBubble();
    }

    void Update()
    {
        if( canUseValve && Input.GetButtonUp( "Fire1" ) )
        {
            GameManager.pressureManager.ActivateValve();
        }
    }
}
