﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ButtonTrigger : MonoBehaviour
{
    public UnityEvent onEnterEvent;
    public UnityEvent onExitEvent;

    void OnTriggerEnter( Collider other )
    {
        onEnterEvent.Invoke();
    }

    void OnTriggerExit( Collider other )
    {
        onExitEvent.Invoke();
    }
}
