﻿using System;
using DG.Tweening;
using UnityEngine;
using System.Collections;

public class PressureManager : MonoBehaviour
{
    //public float valveSpeed = 10;
    public float changeValveStateDuration = 3.0f;
    public float pressureIncreaseSpeed = 100.0f;
    public float pressureShotDecreaseSpeed = 3.0f;
    //public float valveOpeningTime = 2.0f;

    public float timeToDock = 1.0f;

    public float initialPressure = 80.0f;

    public Animator valveAnimator;
    public Transform valveDock;

    private enum ValveState
    {
        CLOSED,
        OPENING,
        OPEN,
        CLOSING
    }

    private ValveState currentValveState = ValveState.CLOSED;

    private float currentPressure = 100.0f;
    private float deltaPressure = 0;

    private bool editPressure = false;

    void Awake()
    {
        currentPressure = initialPressure;
    }


    void Update()
    {
        GameManager.guiManager.pressureBar.fillAmount = currentPressure / 100.0f;

        //if( ( currentValveState == ValveState.OPEN ) || ( currentValveState == ValveState.CLOSED ) )
        //{
        //    GameManager.guiManager.pressureButton.interactable = true;
        //}
        //else
        //{
        //    GameManager.guiManager.pressureButton.interactable = false;
        //}
        if( currentValveState == ValveState.OPEN )
        {
            EditPressure( 1 );
        }
    }

    public string GetDebugMessage()
    {
        string debug_message = "Valve State : " + currentValveState + "\n";

        debug_message += "currentPressure : " + currentPressure + ", deltaPressure : " + deltaPressure;

        return debug_message;
    }

    public void ActivateValve()
    {
        if( currentValveState == ValveState.CLOSED )
        {
            StartCoroutine( OpenPressureValve() );
        }
        else if( currentValveState == ValveState.OPEN )
        {
            StartCoroutine( ClosePressureValve() );
        }
    }

    public IEnumerator OpenPressureValve()
    {
        float start_time = Time.time;

        currentValveState = ValveState.OPENING;
        GameManager.guiManager.pressureButton.interactable = false;
        GameManager.gameManager.player.DisplayBubbleText( "..." );

        float distance_to_dock = Vector3.Distance( valveAnimator.transform.position, GameManager.gameManager.player.transform.position );
        //Dock perso
        GameManager.gameManager.player.transform.DOMove( valveAnimator.transform.position, timeToDock * distance_to_dock );
        GameManager.gameManager.player.transform.DORotate( valveDock.eulerAngles, timeToDock * distance_to_dock );
        //GameManager.gameManager.player.transform.rotation = valveDock.rotation;
        yield return new WaitForSeconds( timeToDock * distance_to_dock );

        valveAnimator.SetTrigger( "use" );
        GameManager.gameManager.player.GetComponent<Animator>().SetTrigger( "valve_on" );

        while( Time.time - start_time < changeValveStateDuration )
        {
            EditPressure( 1 );
            yield return null;
        }

        currentValveState = ValveState.OPEN;
        GameManager.guiManager.pressureButton.interactable = true;
        GameManager.gameManager.player.DisplayBubbleText( GameManager.gameManager.activableText );
    }

    public IEnumerator ClosePressureValve()
    {
        float start_time = Time.time;

        currentValveState = ValveState.CLOSING;
        GameManager.guiManager.pressureButton.interactable = false;
        GameManager.gameManager.player.DisplayBubbleText( "..." );

        valveAnimator.SetTrigger( "use" );
        GameManager.gameManager.player.GetComponent<Animator>().SetTrigger( "valve_off" );

        while( Time.time - start_time < changeValveStateDuration )
        {
            EditPressure( 0 );
            yield return new WaitForEndOfFrame();
        }

        currentValveState = ValveState.CLOSED;
        GameManager.guiManager.pressureButton.interactable = true;
        GameManager.gameManager.player.DisplayBubbleText( GameManager.gameManager.activableText );
    }

    public void InteractPressure()
    {
        string str = "";
        float moveH = Input.GetAxis( "Horizontal" );
        bool spaceB = Input.GetButtonDown( "Jump" );
        if( editPressure )
        {
            EditPressure( moveH );
            if( moveH > 0 )
            {
                str = "Right";
            }
            if( moveH < 0 )
            {
                str = "Left";
            }
            if( spaceB )
            {
                editPressure = false;
                deltaPressure = 0;
            }
        }
        else
        {
            str = "Press Space to Edit Pressure";
            if( spaceB )
            {
                editPressure = true;
            }
        }
        GameManager.gameManager.player.DisplayBubbleText( str );
    }

    void EditPressure( float moveH )
    {
        moveH = Math.Abs( moveH );
        if( deltaPressure > 1 )
            deltaPressure = 1;
        if( deltaPressure < 0 )
            deltaPressure = 0;
        if( moveH != 0 )
            deltaPressure += moveH * Time.deltaTime;
        else
            deltaPressure -= Time.deltaTime;

        currentPressure = Mathf.Clamp( currentPressure, 0, 100 );

        currentPressure += ( float ) ( Math.Round( deltaPressure, 1 ) * Time.deltaTime * pressureIncreaseSpeed );
        /*/
        if (deltaPressure < 0.1 && deltaPressure > 0)
            deltaPressure = 0;
        if (deltaPressure > -0.1 && deltaPressure < 0)
            deltaPressure = 0;
        //*/
    }

    public bool GetEditPressure()
    {
        return editPressure;
    }

    public void ReducePressure( float lost_pressure )
    {
        currentPressure -= lost_pressure;
        Mathf.Clamp( currentPressure, 0, 100 );
    }

    public float GetCurrentPressure()
    {
        return currentPressure;
    }

    public float GetPressureModifier()
    {
        float pressure_modifier;

        currentPressure = Mathf.Clamp( currentPressure, 1, 100 );

        pressure_modifier = 100.0f / currentPressure;

        return pressure_modifier;
    }
}
