﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{
    public Text debugText;
    public Image debugPanel;
    public Image gameOverPanel;
    public Image gameStartPanel;
    public InputField gameStartScreenInputField;
    public Text scoreText;
    public Image pressureBar;
    public Button pressureButton;

    public Button startLoadingButton;
    public List<Button> loadingButtonTable;

    public Text controlScreenText;

    void Update()
    {
        string info_message = "";

        info_message = "Master Control \n";

        info_message += "------------------\n";

        info_message += GameManager.gameManager.waveInfo + "\n";

        info_message += "Estim. Canon Dirt - " + Mathf.FloorToInt( GameManager.gameManager.GetDirtyness() / 10.0f ) + "/10\n";
        info_message += "Estim. Steam Pressure - " + Mathf.RoundToInt( GameManager.pressureManager.GetCurrentPressure() / 10.0f ) + "/10\n";
        info_message += "Estim. Canon Damage - " + ( 10 - Mathf.RoundToInt( ( GameManager.gameManager.GetCurrentLife() / GameManager.gameManager.initialLifeCount ) * 10 ) ) + "/10\n";

        controlScreenText.text = info_message;
    }
}
